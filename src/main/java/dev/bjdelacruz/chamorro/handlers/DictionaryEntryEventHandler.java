package dev.bjdelacruz.chamorro.handlers;

import dev.bjdelacruz.chamorro.models.DictionaryEntry;
import org.springframework.data.rest.core.annotation.HandleBeforeCreate;
import org.springframework.data.rest.core.annotation.HandleBeforeSave;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.stereotype.Component;

import jakarta.validation.ConstraintViolationException;
import java.util.HashSet;

@Component
@RepositoryEventHandler
public class DictionaryEntryEventHandler {

    @HandleBeforeCreate
    @HandleBeforeSave
    public void validateEntry(DictionaryEntry entry) {
        if (entry.getEntry() == null || entry.getEntry().isEmpty()) {
            throw new ConstraintViolationException("Entry cannot be null or empty.", new HashSet<>());
        }

        if (entry.getMeaning() == null || entry.getMeaning().isEmpty()) {
            throw new ConstraintViolationException("Meaning cannot be null or empty.", new HashSet<>());
        }

        if (entry.getEntry().length() > 100
                || (entry.getPronunciation() != null && entry.getPronunciation().length() > 100)) {
            throw new ConstraintViolationException("Entry or pronunciation cannot be greater than 100 characters.",
                    new HashSet<>());
        }

        if (entry.getMeaning().length() > 1000
                || (entry.getEtymology() != null && entry.getEtymology().length() > 1000)
                || (entry.getExamples() != null && entry.getExamples().length() > 1000)
                || (entry.getComments() != null && entry.getComments().length() > 1000)) {
            throw new ConstraintViolationException(
                    "Etymology, meaning, examples, or comments cannot be greater than 1000 characters.",
                    new HashSet<>());
        }
    }
}
