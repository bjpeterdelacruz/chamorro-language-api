package dev.bjdelacruz.chamorro.configs;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import jakarta.validation.ConstraintViolationException;
import java.util.Date;

@ControllerAdvice
public class ControllerConfiguration extends ResponseEntityExceptionHandler {

    static class HttpError {
        private final Date timestamp;
        private final int statusCode;
        private final String reasonPhrase;
        private final String message;

        HttpError(int statusCode, String reasonPhrase, String message) {
            this.timestamp = new Date();
            this.statusCode = statusCode;
            this.reasonPhrase = reasonPhrase;
            this.message = message;
        }

        public Date getTimestamp() {
            return timestamp;
        }

        public int getStatusCode() {
            return statusCode;
        }

        public String getReasonPhrase() {
            return reasonPhrase;
        }

        public String getMessage() {
            return message;
        }
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<?> handleException(ConstraintViolationException ex) {
        var error = new HttpError(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.getReasonPhrase(),
                ex.getMessage());
        return new ResponseEntity<>(error, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }
}
