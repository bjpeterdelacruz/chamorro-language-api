/**
 * Contains the configurations for this Spring Boot application.
 */
package dev.bjdelacruz.chamorro.configs;
