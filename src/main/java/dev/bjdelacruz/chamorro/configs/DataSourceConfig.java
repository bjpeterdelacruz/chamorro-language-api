package dev.bjdelacruz.chamorro.configs;

import com.azure.identity.DefaultAzureCredentialBuilder;
import com.azure.security.keyvault.secrets.SecretClient;
import com.azure.security.keyvault.secrets.SecretClientBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

@Configuration
public class DataSourceConfig {

    @Value("${DB_URL}")
    private String url;

    @Value("${DB_USERNAME}")
    private String username;

    @Value("${DB_PASSWORD}")
    private String password;

    @Bean
    public DataSource dataSource() {
        var className = "com.microsoft.sqlserver.jdbc.SQLServerDriver";

        var keyVaultUrl = "https://chamorro-language-api-kv.vault.azure.net/";

        var secretClient = new SecretClientBuilder()
                .vaultUrl(keyVaultUrl)
                .credential(new DefaultAzureCredentialBuilder().build())
                .buildClient();

        var url = getSecret(secretClient, "db-url", this.url);
        var username = getSecret(secretClient, "db-username", this.username);
        var password = getSecret(secretClient, "db-password", this.password);

        var dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(className);
        dataSource.setUrl(url);
        dataSource.setUsername(username);
        dataSource.setPassword(password);
        return dataSource;
    }

    private static String getSecret(SecretClient secretClient, String secretName, String value) {
        return value == null || value.isBlank() ? secretClient.getSecret(secretName).getValue()
                : value;
    }
}
