package dev.bjdelacruz.chamorro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChamorroLanguageApplication {

    public static void main(String[] args) {
        SpringApplication.run(ChamorroLanguageApplication.class, args);
    }
}
