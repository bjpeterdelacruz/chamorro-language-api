package dev.bjdelacruz.chamorro.repositories;

import dev.bjdelacruz.chamorro.models.DictionaryEntry;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

@RepositoryRestResource(collectionResourceRel = "entries", itemResourceRel = "entry", path = "entries")
public interface DictionaryEntryRepository extends JpaRepository<DictionaryEntry, Long> {

    @RestResource(path = "findByEntryEquals", rel = "findByEntryEquals")
    Page<DictionaryEntry> findByEntryEquals(String equals, Pageable pageable);

    @RestResource(path = "findByEntryStartsWith", rel = "findByEntryStartsWith")
    Page<DictionaryEntry> findByEntryStartsWithIgnoreCase(String startsWith, Pageable pageable);

    @RestResource(path = "findByEntryContains", rel = "findByEntryContains")
    Page<DictionaryEntry> findByEntryContainsIgnoreCase(String contains, Pageable pageable);

    @RestResource(path = "findByMeaningContains", rel = "findByMeaningContains")
    Page<DictionaryEntry> findByMeaningContainsIgnoreCase(String contains, Pageable pageable);

    @RestResource(path = "findByExamplesContains", rel = "findByExamplesContains")
    Page<DictionaryEntry> findByExamplesContainsIgnoreCase(String contains, Pageable pageable);

    @RestResource(path = "findByCommentsContains", rel = "findByCommentsContains")
    Page<DictionaryEntry> findByCommentsContainsIgnoreCase(String contains, Pageable pageable);

    @RestResource(exported = false)
    @Override
    DictionaryEntry save(DictionaryEntry entry);

    @RestResource(exported = false)
    @Override
    void delete(DictionaryEntry entry);

    @RestResource(exported = false)
    @Override
    void deleteById(Long id);

    @RestResource(exported = false)
    @Override
    void deleteAllById(Iterable<? extends Long> ids);

    @RestResource(exported = false)
    @Override
    void deleteAll(Iterable<? extends DictionaryEntry> entries);

    @RestResource(exported = false)
    @Override
    void deleteAll();
}
