/**
 * Contains the JPA repositories for this Spring Boot application.
 */
package dev.bjdelacruz.chamorro.repositories;
