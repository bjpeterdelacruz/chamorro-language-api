package dev.bjdelacruz.chamorro.models;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import java.util.Date;

@Entity
@Table(name = "ENTRIES")
public class DictionaryEntry {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    @Size(max = 100)
    @Column(nullable = false, length = 100)
    private String entry;

    @Size(max = 100)
    @Column(length = 100)
    private String pronunciation;

    @NotNull
    @Size(max = 1000)
    @Column(nullable = false, length = 1000)
    private String meaning;

    @Size(max = 1000)
    @Column(length = 1000)
    private String etymology;

    @Size(max = 1000)
    @Column(length = 1000)
    private String examples;

    @Size(max = 1000)
    @Column(length = 1000)
    private String comments;

    @CreationTimestamp
    @Column(name = "CREATED_ON", nullable = false, updatable = false)
    private Date createdOn;

    @UpdateTimestamp
    @Column(name = "LAST_UPDATED_ON", nullable = false)
    private Date lastUpdatedOn;

    public DictionaryEntry() {
        var date = new Date();
        createdOn = date;
        lastUpdatedOn = date;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEntry() {
        return entry;
    }

    public void setEntry(String entry) {
        this.entry = entry;
    }

    public String getPronunciation() {
        return pronunciation;
    }

    public void setPronunciation(String pronunciation) {
        this.pronunciation = pronunciation;
    }

    public String getMeaning() {
        return meaning;
    }

    public void setMeaning(String meaning) {
        this.meaning = meaning;
    }

    public String getEtymology() {
        return etymology;
    }

    public void setEtymology(String etymology) {
        this.etymology = etymology;
    }

    public String getExamples() {
        return examples;
    }

    public void setExamples(String examples) {
        this.examples = examples;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Date getCreatedOn() {
        return new Date(createdOn.getTime());
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = new Date(createdOn.getTime());
    }

    public Date getLastUpdatedOn() {
        return new Date(lastUpdatedOn.getTime());
    }

    public void setLastUpdatedOn(Date lastUpdatedOn) {
        this.lastUpdatedOn = new Date(lastUpdatedOn.getTime());
    }
}
