package dev.bjdelacruz.chamorro.handlers;

import dev.bjdelacruz.chamorro.models.DictionaryEntry;
import org.junit.jupiter.api.Test;
import jakarta.validation.ConstraintViolationException;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class DictionaryEntryEventHandlerTest {

    private final DictionaryEntryEventHandler handler = new DictionaryEntryEventHandler();

    @Test
    public void testValidateEntry_ValidObject_MandatoryFields() {
        var entry = new DictionaryEntry();
        entry.setEntry("Hafa Adai");
        entry.setMeaning("Hello");
        handler.validateEntry(entry);
    }

    @Test
    public void testValidateEntry_ValidObject_OptionalFields() {
        var entry = new DictionaryEntry();
        entry.setEntry("Hafa Adai");
        entry.setPronunciation("Half a day");
        entry.setMeaning("Hello");
        entry.setEtymology("N/A");
        entry.setExamples("Hafa Adai & Tirow!");
        entry.setComments("It's used the same way as Aloha in Hawaiian.");
        handler.validateEntry(entry);
    }

    @Test
    public void testValidateEntry_InvalidEntry() {
        assertThatThrownBy(() -> handler.validateEntry(new DictionaryEntry())).isInstanceOf(ConstraintViolationException.class);
        assertThatThrownBy(() -> {
            var entry = new DictionaryEntry();
            entry.setEntry("");
            handler.validateEntry(entry);
        }).isInstanceOf(ConstraintViolationException.class);
    }

    @Test
    public void testValidateEntry_InvalidMeaning() {
        assertThatThrownBy(() -> {
            var entry = new DictionaryEntry();
            entry.setEntry("Hafa Adai");
            handler.validateEntry(entry);
        }).isInstanceOf(ConstraintViolationException.class);
        assertThatThrownBy(() -> {
            var entry = new DictionaryEntry();
            entry.setEntry("Hafa Adai");
            entry.setMeaning("");
            handler.validateEntry(entry);
        }).isInstanceOf(ConstraintViolationException.class);
    }

    @Test
    public void testValidateEntry_GreaterThan100() {
        var longString = IntStream.range(0, 100).mapToObj(Integer::toString)
                .collect(Collectors.joining());

        assertThatThrownBy(() -> {
            var entry = new DictionaryEntry();
            entry.setEntry(longString);
            entry.setMeaning("Hello");
            handler.validateEntry(entry);
        }).isInstanceOf(ConstraintViolationException.class);
        assertThatThrownBy(() -> {
            var entry = new DictionaryEntry();
            entry.setEntry("Hafa Adai");
            entry.setMeaning("Hello");
            entry.setPronunciation(longString);
            handler.validateEntry(entry);
        }).isInstanceOf(ConstraintViolationException.class);
    }

    @Test
    public void testValidateEntry_GreaterThan1000() {
        var longString = IntStream.range(0, 1000).mapToObj(Integer::toString)
                .collect(Collectors.joining());

        assertThatThrownBy(() -> {
            var entry = new DictionaryEntry();
            entry.setEntry("Hafa Adai");
            entry.setMeaning(longString);
            handler.validateEntry(entry);
        }).isInstanceOf(ConstraintViolationException.class);
        assertThatThrownBy(() -> {
            var entry = new DictionaryEntry();
            entry.setEntry("Hafa Adai");
            entry.setMeaning("Hello");
            entry.setEtymology(longString);
            handler.validateEntry(entry);
        }).isInstanceOf(ConstraintViolationException.class);
        assertThatThrownBy(() -> {
            var entry = new DictionaryEntry();
            entry.setEntry("Hafa Adai");
            entry.setMeaning("Hello");
            entry.setExamples(longString);
            handler.validateEntry(entry);
        }).isInstanceOf(ConstraintViolationException.class);
        assertThatThrownBy(() -> {
            var entry = new DictionaryEntry();
            entry.setEntry("Hafa Adai");
            entry.setMeaning("Hello");
            entry.setComments(longString);
            handler.validateEntry(entry);
        }).isInstanceOf(ConstraintViolationException.class);
    }
}
