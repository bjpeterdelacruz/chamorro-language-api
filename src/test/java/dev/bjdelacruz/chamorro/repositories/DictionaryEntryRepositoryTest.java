package dev.bjdelacruz.chamorro.repositories;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.PageRequest;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

import static org.assertj.core.api.Assertions.assertThat;

@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@DataJpaTest
@Import(DictionaryEntryRepositoryTest.MySqlDataSourceConfig.class)
public class DictionaryEntryRepositoryTest {

    @Autowired
    private DictionaryEntryRepository repository;

    @Test
    public void testFindByEntryEquals() {
        var actual = repository.findByEntryEquals("a'amte", PageRequest.of(0, 10));
        assertThat(actual.getTotalElements()).isEqualTo(1L);
    }

    @Test
    public void testFindByEntryStartsWithIgnoreCase() {
        var actual = repository.findByEntryStartsWithIgnoreCase("A'", PageRequest.of(0, 25));
        assertThat(actual.getTotalElements()).isEqualTo(21L);

        actual.getContent().forEach(entry ->
                Assertions.assertThat(entry.getEntry()).startsWith("a'"));
    }

    @Test
    public void testFindByEntryContainsIgnoreCase() {
        var actual = repository.findByEntryContainsIgnoreCase("fat", PageRequest.of(0, 20));
        assertThat(actual.getTotalElements()).isEqualTo(17L);

        actual.getContent().forEach(entry ->
                Assertions.assertThat(entry.getEntry()).contains("fat"));
    }

    @TestConfiguration
    static class MySqlDataSourceConfig {
        @Value("${DB_URL}")
        private String url;

        @Value("${DB_USERNAME}")
        private String username;

        @Value("${DB_PASSWORD}")
        private String password;

        @Value("${spring.datasource.driverClassName}")
        private String driverClassName;

        @Bean
        public DataSource dataSource() {
            var dataSource = new DriverManagerDataSource();
            dataSource.setDriverClassName(driverClassName);
            dataSource.setUrl(url);
            dataSource.setUsername(username);
            dataSource.setPassword(password);
            return dataSource;
        }
    }

}
