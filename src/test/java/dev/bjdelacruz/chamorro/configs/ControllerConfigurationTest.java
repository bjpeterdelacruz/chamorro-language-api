package dev.bjdelacruz.chamorro.configs;

import dev.bjdelacruz.chamorro.handlers.DictionaryEntryEventHandler;
import dev.bjdelacruz.chamorro.models.DictionaryEntry;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import jakarta.validation.ConstraintViolationException;

import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

public class ControllerConfigurationTest {

    @Test
    public void testExceptionHandler() {
        var entry = new DictionaryEntry();
        var handler = new DictionaryEntryEventHandler();
        try {
            handler.validateEntry(entry);
        }
        catch (ConstraintViolationException cve) {
            var config = new ControllerConfiguration();
            var actual = config.handleException(cve);
            var httpError = (ControllerConfiguration.HttpError) actual.getBody();
            assertThat(httpError).isNotNull();
            assertThat(httpError.getTimestamp()).isBefore(new Date());
            assertThat(httpError.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST.value());
            assertThat(httpError.getReasonPhrase()).isEqualTo(HttpStatus.BAD_REQUEST.getReasonPhrase());
            assertThat(httpError.getMessage()).isEqualTo("Entry cannot be null or empty.");
            assertThat(actual.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        }
    }
}
