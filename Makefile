DOCKER=docker

all: build_and_run

stop_and_rm:
	if $(DOCKER) ps -a | grep -q chamorro-dictionary-mssql-server; \
	  then $(DOCKER) stop chamorro-dictionary-mssql-server && $(DOCKER) rm chamorro-dictionary-mssql-server; \
	fi
	if $(DOCKER) ps -a | grep -q chamorro-language-api-app; \
	  then $(DOCKER) stop chamorro-language-api-app && $(DOCKER) rm chamorro-language-api-app; \
	fi

run_mssql_server: create_network
	$(DOCKER) run --net local --ip="10.1.0.104" -dit --name chamorro-dictionary-mssql-server \
	  -p 1433:1433 bjdelacruz/chamorro-dictionary-mssql-server

build_and_run: create_network stop_and_rm run_mssql_server
	./gradlew clean build && $(DOCKER) build -t chamorro-language-api --platform linux/amd64 . && \
	  $(DOCKER) run --net local --ip="10.1.0.102" -dit --name chamorro-language-api-app \
	  -e AZURE_SQL_DATABASE_URL="${CHAMORRO_DICTIONARY_SQL_SERVER_URL}" \
	  -e AZURE_SQL_DATABASE_USERNAME=${CHAMORRO_DICTIONARY_SQL_SERVER_USERNAME} \
	  -e AZURE_SQL_DATABASE_PASSWORD=${CHAMORRO_DICTIONARY_SQL_SERVER_PASSWORD} \
	  -p 9000:9000 chamorro-language-api

build:
	./gradlew clean build && $(DOCKER) build -t chamorro-language-api --platform linux/amd64 .

create_network:
	$(DOCKER) network inspect local | grep -q 10.1.0.0 || \
	  $(DOCKER) network create local --subnet=10.1.0.0/16
