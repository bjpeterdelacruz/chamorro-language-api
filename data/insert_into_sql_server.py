import pyodbc
import os

DB_CONFIG = {
    "server": "localhost",
    "database": "chamorrodictionary",
    "username": "sa",
    "password": os.environ["CHAMORRO_DICTIONARY_SQL_SERVER_SA_PASSWORD"],
    "driver": "{ODBC Driver 18 for SQL Server}"
}

SQL_FILE_PATH = "sqlserver-data.sql"

def connect_to_db():
    """Establishes connection to SQL Server."""
    try:
        conn = pyodbc.connect(
            f"DRIVER={DB_CONFIG['driver']};"
            f"SERVER={DB_CONFIG['server']};"
            f"DATABASE={DB_CONFIG['database']};"
            f"UID={DB_CONFIG['username']};"
            f"PWD={DB_CONFIG['password']};TrustServerCertificate=YES"
        )
        return conn
    except Exception as e:
        print(f"Error connecting to the database: {e}")
        return None

def execute_insert_statements(file_path):
    """Reads INSERT statements from a file and executes them one by one."""
    conn = connect_to_db()
    if not conn:
        return
    
    cursor = conn.cursor()

    try:
        count = 0
        with open(file_path, "r", encoding="utf-8") as file:
            while True:
                line = file.readline()
                if not line:
                    break
                sql = line.strip()
                if sql:
                    try:
                        cursor.execute(sql)
                        count += 1
                        if count % 10000 == 0:
                            print(f"{count}: Successfully executed INSERT statements...")
                    except Exception as err:
                        print(f"{count + 1}: An error was encountered while executing {sql}... -> {err}")

        conn.commit()
        print("All INSERT statements executed successfully!")

    except FileNotFoundError:
        print(f"SQL file not found: {file_path}")
    
    finally:
        cursor.close()
        conn.close()

execute_insert_statements(SQL_FILE_PATH)
