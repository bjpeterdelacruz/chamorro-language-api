CREATE DATABASE chamorrodictionary;
GO

USE chamorrodictionary;
GO

CREATE TABLE ENTRIES (
    ID INT IDENTITY (1, 1) NOT NULL,
    ENTRY VARCHAR(100) NOT NULL,
    PRONUNCIATION VARCHAR(100),
    MEANING VARCHAR(1000) NOT NULL,
    ETYMOLOGY VARCHAR(1000),
    EXAMPLES VARCHAR(1000),
    COMMENTS VARCHAR(1000),
    CREATED_ON DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
    LAST_UPDATED_ON DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL);
GO

CREATE LOGIN chamorrolanguageapi WITH PASSWORD = 'EAojo6pDRknS87J6';
CREATE USER chamorrolanguageapi FOR LOGIN chamorrolanguageapi;
GRANT SELECT ON dbo.ENTRIES TO chamorrolanguageapi;
GO
