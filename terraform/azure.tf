terraform {
  backend "s3" {
    bucket = "dev-bjdelacruz-terraform"
    key    = "chamorro-language-api/state.tfstate"
    region = "us-east-2"
  }
}

provider "azurerm" {
  features {}
}

locals {
  app_name              = "chamorro-language-api"
  app_service_plan_name = "${local.app_name}-asp"
  docker_image          = "bjdelacruz/chamorro-language-api"
  key_vault_name        = "${local.app_name}-kv"
  resource_group_name   = "rg-${local.app_name}"
  web_app_name          = "${local.app_name}-web-app"
}

variable "location" {
  default = "West US"
}

# Resource Group
resource "azurerm_resource_group" "main" {
  name     = local.resource_group_name
  location = var.location
}

# Data source for Azure credentials
data "azurerm_client_config" "current" {}

# Key Vault
resource "azurerm_key_vault" "kv" {
  name                     = local.key_vault_name
  location                 = azurerm_resource_group.main.location
  resource_group_name      = azurerm_resource_group.main.name
  tenant_id                = data.azurerm_client_config.current.tenant_id
  sku_name                 = "standard"
  purge_protection_enabled = true
}

variable "AZURE_SQL_DATABASE_URL" {}

variable "AZURE_SQL_DATABASE_USERNAME" {}

variable "AZURE_SQL_DATABASE_PASSWORD" {}

# Add Key Vault Secrets
resource "azurerm_key_vault_secret" "db_url" {
  name         = "db-url"
  value        = var.AZURE_SQL_DATABASE_URL
  key_vault_id = azurerm_key_vault.kv.id

  depends_on = [
    azurerm_key_vault_access_policy.kv_ap_current_user
  ]
}

resource "azurerm_key_vault_secret" "db_username" {
  name         = "db-username"
  value        = var.AZURE_SQL_DATABASE_USERNAME
  key_vault_id = azurerm_key_vault.kv.id

  depends_on = [
    azurerm_key_vault_access_policy.kv_ap_current_user
  ]
}

resource "azurerm_key_vault_secret" "db_password" {
  name         = "db-password"
  value        = var.AZURE_SQL_DATABASE_PASSWORD
  key_vault_id = azurerm_key_vault.kv.id

  depends_on = [
    azurerm_key_vault_access_policy.kv_ap_current_user
  ]
}

# Grant current user access to Key Vault
resource "azurerm_key_vault_access_policy" "kv_ap_current_user" {
  key_vault_id = azurerm_key_vault.kv.id
  tenant_id    = data.azurerm_client_config.current.tenant_id
  object_id    = data.azurerm_client_config.current.object_id

  secret_permissions = [
    "Get",
    "List",
    "Set",
    "Delete",
    "Recover",
    "Backup",
    "Restore"
  ]
}

# Grant App Service access to Key Vault
resource "azurerm_key_vault_access_policy" "kv_ap_app_service" {
  key_vault_id = azurerm_key_vault.kv.id
  tenant_id    = data.azurerm_client_config.current.tenant_id
  object_id    = azurerm_linux_web_app.main.identity[0].principal_id

  secret_permissions = [
    "Get",
    "List"
  ]
}

# App Service Plan
resource "azurerm_service_plan" "main" {
  name                = local.app_service_plan_name
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name
  os_type             = "Linux"
  sku_name            = "F1"
}

# Web App for Containers
resource "azurerm_linux_web_app" "main" {
  name                = local.web_app_name
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name
  service_plan_id     = azurerm_service_plan.main.id
  https_only          = true

  site_config {
    always_on = false

    application_stack {
      docker_registry_url = "https://registry.hub.docker.com"
      docker_image_name   = local.docker_image
    }
  }

  app_settings = {
    "WEBSITES_ENABLE_APP_SERVICE_STORAGE" = "false"
    "DOCKER_ENABLE_CI"                    = "true"
    "DB_URL"                              = "@Microsoft.KeyVault(SecretUri=${azurerm_key_vault_secret.db_url.id})"
    "DB_USERNAME"                         = "@Microsoft.KeyVault(SecretUri=${azurerm_key_vault_secret.db_username.id})"
    "DB_PASSWORD"                         = "@Microsoft.KeyVault(SecretUri=${azurerm_key_vault_secret.db_password.id})"
    "WEBSITES_PORT"                       = 9000
  }

  identity {
    type = "SystemAssigned"
  }
}
