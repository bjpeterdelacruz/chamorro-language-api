FROM openjdk:23-oracle
WORKDIR /app
COPY build/libs/*.jar /app/chamorro.jar
CMD ["sh", "-c", "java -jar chamorro.jar"]
