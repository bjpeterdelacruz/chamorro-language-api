# Chamorro Language API

This **read-only** REST API provides clients with a list of Chamorro words, their English meanings, etymology, and
pronunciation.

## Live Demo

### API

[https://chamorro-language-api-web-app.azurewebsites.net/api/entries](https://chamorro-language-api-web-app.azurewebsites.net/api/entries)

### Web Application

[https://chamorro-dictionary-web-knartlettq-uw.a.run.app](https://chamorro-dictionary-web-knartlettq-uw.a.run.app)

## Developer Guide

### Pre-requisites

* Java Development Kit, version 23 or newer
* Docker Desktop, version 4.26.1 or newer

### Setting Up the Infrastructure in Microsoft Azure

Execute the Terraform script located in the `terraform` directory to stand up the infrastructure for
hosting the Docker container on a free tier of App Service.

### Running Unit Tests and Building an Executable JAR File

```
gradlew clean build
```

### Building a Docker Image

Use the `make` command to build a Docker image and create a running container:

```
make
```

### Downloading the Latest Docker Image

```
docker pull bjdelacruz/chamorro-language-api:latest
```

### Building a Docker Image for SQL Server

1. Run an instance of SQL Server 2022 in a container: `docker run --name=chamorro-dictionary-mssql-server -e "ACCEPT_EULA=Y" -e "MSSQL_SA_PASSWORD=<PASSWORD>" -p 1433:1433 -d mcr.microsoft.com/mssql/server:2022-latest`

2. Connect to the container: `docker exec -it chamorro-dictionary-mssql-server bash`

3. Login to SQL Server and execute all the statements in the `sqlserver.sql` script located in the `data` directory to
create the `chamorrodictionary` database and the `ENTRIES` table: `/opt/mssql-tools18/bin/sqlcmd -S localhost -U sa -P '<PASSWORD>' -No`

4. Execute the `insert_into_sql_server.py` script located in the `data` directory to populate the `PRIMES` table.

5. Create an image from the running container: `docker commit <CONTAINER ID> chamorro-dictionary-mssql-server`

6. (Optional) Tag the new image, e.g.: `docker tag chamorro-dictionary-mssql-server <NAMESPACE>/chamorro-dictionary-mssql-server`

7. Run `make` to build a bootable JAR file and create two containers, one running the SQL Server instance containing the
populated `primes` database (`chamorro-dictionary-mssql-server`) and another running the API (`chamorro-language-api`).

## See Also

[Chamorro Dictionary](https://bitbucket.org/bjpeterdelacruz/chamorro-dictionary-web)

[Chamorro Language Reference](https://bitbucket.org/bjpeterdelacruz/chamorro-language-reference):
a desktop version of Chamorro Dictionary that can run on macOS and Windows devices